# The Settings File

**eliosin**'s settings file is intentionally small. Some are edicts, like the dimensions of the _prong_. Others, like `$god-space` are best thought as the last words spoken by a soccer coach to her players before they take to the field; more "do your best, I have faith in you" than "you must pass the ball within 6 seconds of receiving it".

Few as they are, it's best to group them:

## Layout Settings

All sizes should be given in percentages. This is the best way to ensure the layout scales naturally with screenWidths. % sizing units are the "I have faith in you" of the stylesheet.

By default in `HMTL`, the fontSize of `h6` is scaled at 100% of the body fontSize. We suggest you keep `$god-h6` set to 100% so that it scales consistently with body text, and `h5`+ will remain _fibonaccially_ larger than `h5` and so on.

### `$god-space`

Used for verticalPadding between **god**'s three-column _prong_. Don't worry about it: **god** uses this as a starting point and adjusts the padding depending on the position and mediaWidth. Something like 0.2% to 5% would be a reasonable range. Artsy folks should turn the dial to 11%.

### `$heaven-width`

The width of the gutter right of the _pillar_.

### `$hell-width`

The width of the gutter left of the _pillar_.

NB: **pillar-width** is not an explicit setting, but is dynamically derived from the `$hell-width`, `$heaven-width` and `$god-space` and will adjust automatically for mediaWidths.

### `$personal-space`

A little bit of extra space you can give tagNames who might need the room.

### `$transition`

The transition speed used for all transitions. We make this a setting so that all animations are consistent. You can make your application feel snappy or lazy.

## Media Widths Settings

### `$god-loose`

**god** will layout _prongs_ nice and loose, making full use of `$god-space`.

### `$god-cosy`

At screenWidth's `$god-cosy` or below **god** will layout _prongs_ with some overlap between sinners and the pillar..

### `$god-tight`

The _prong_ collapses. _heaven_ and _hell_ join the _pillar_ down the center of what is probably a phone's screen at this size. The _pillar_ maintains a slight margin from the screen edge, while _heaven_ and _hell_ fit 100% the screen width.

## Color Settings

### `$heaven-color`

Color scheme of the boxes on the right. Choose something _righteous_ and contrasting in hue, contrast or brightness with `$hell-color`. Whether this color becomes the `color:` or the `background-color:` depends on your style.

### `$pillar-color`

The pillar text color, but you can use it differently. Should contrast with white.

### `$hell-color`

Color scheme of boxes on the left. Choose something _sinful_ and contrasting in hue, contrast or brightness with `$heaven-color`. Whether this color becomes the `color:` or the `background-color:` depends on your style.

### `$god-color`

There are three colors. To keep with good design practices, **god** recommends you take a two-tone approach. Decide whether your theme is more _sinful_ or _righteous_ and make the `$god-color` some version of `$hell-color` or `$heaven-color` accordingly. You will use the `$god-color` to harmonize elements sitting in those opposites sides, _hell_ or _heaven_.

### `$god-blushed`

A lighter tint of `$god-color`.

## Font Settings

**god** automatically scales all the other heading fontSizes in-between `$god-h1` and `$god-h6` using `SassyFibonacciness`. At smaller screen sizes the whole range will also be adjusted - using _fibonacciness_ to drop the fontSize of `h1` _fiboproportinally_.

### `$god-h1`

The fontSize as a percentage of `h1` at the largest screen width.

### `$god-h6`

The size of `h6` and `body` text at the largest screen width.

### `$god-spoke`

A larger body size for jumbotron or lead text; primary buttons, etc.

### `$god-head`

Font family for heading level text.

### `$god-body`

Font family for body level text.

## Basic Settings

### `$god-border`

A border, if you want, for tables, fieldsets, buttons, etc. Default: `1px $god-blushed solid`

### `$inline-padding`

Small horizontal vertical padding to be used in table cells and inputs. Default `3px`

### `$toothpaste-color`

Optional: A colour for a table toothpaste effect. Default: `none`

## Prongs and Believers

These settings feed into the main engine of **god** . They should contain a simple comma separated list of tagNames. It isn't expected that you would put all tagNames in these settings - just the ones you want to form the wireframe.

### `$container`

The parent of tagNames(s). Tag names listed in _prongs_ must be direct children of `$container`, otherwise they will be treated as _atheists_. Atheists are fine. They will just do what they do normally - as HTML - unthemed in the context they find themselves. All grandchildren of the `$container` are ignored by **god** and themed by **eve** optionally.

If your theme is driving a Single Page App, the natural `$container` would be your app's parent DOM.

It's worth noting that because **eliosin** themes work inside a single, main tag, it plays nicely with other CSS frameworks. You can even have two different themes running on the same page, as longs as the `$container` is different. A css class name to distinguish them would be sinful. That's okay.

`body` or `main` are recommended by **god** . `main` has the advantage that it behaves like `body` when nested in `body`.

```scss
// example1
$container: (
  body, main
)

// example2
$container: (
  body, body>main
)
```

`main` and `body>main` are fundamentally different because the latter means `p`s 4=>6 won't pick up the `$pillar` style in the example shown):

```html
<body>
  <main>
    <p>1</p>
    <p>2</p>
    <p>3</p>
    <main>
      <p>4</p>
      <p>5</p>
      <p>6</p>
    </main>
  </main>
</body>
```

### `$pillar`

The tagNames(s) on this page that you want in the _pillar_.

### `$heaven`

The tagNames(s) on this page to be floated to the right of the _pillar_.

### `$hell`

The tagNames(s) on this page to be floated to the left of the _pillar_.

Any other tagNames, which you intend to use nested inside a _believer_ tag, should be left out of the settings file. You might, for instance, wish your `table`'s to be nested inside a `p` tag firmly placed in the _pillar_. Arguably though there is no reason why table could not be added to the _pillar_. In the cases where you _do_ nest `table` inside `p` - it will not be affected by the **eliosin** stylesheet because it is not a direct child.
