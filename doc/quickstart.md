# Quickstart god

- [god Prerequisites](/eliosin/god/prerequisites.html)
- [Installing god](/eliosin/god/installing.html)

## Nutshell

- [Check the Demo](/eliosin/god/demo.html)

To start a new **eliosin** theme.

1. Create a new folder for your project.
2. Run `yo sin` in that folder.
3. Follow the instructions when prompted.
4. Type `gulp`.
5. Change the `settings.scss` file to taste.

## Getting Started

```shell
mkdir genesis
cd genesis
```

- Choose a name for your **eliosin** project.

  - We will use the name `genesis` for all **eliosin** quickstarts.

- Create the folder somewhere and `cd` into it.

## Exercise 1 Call the sinful yeoman generator

```shell
yo sin
```

- Run the **eliosin** generator using the `yo sin` command.

- Accept the defaults to every question (as shown).

```shell
? Tags going into the pillar h1, h2, h3, h4, p, ul, ol, dl, table,
? Tags bound for heaven blockquote, h6, summary,
? Tags sent to hell nav, aside, details,
```

- Now wait...

  - ...Files are created...
  - ...The generator runs the `yarn` command to install project's requirements...
  - ...The finished message appears.

    - [More about yo sin prompts](/eliosin/generator-sin/prompts.html)

### Important things generator-sin makes

```shell
project
│   index.html
│   README.md
│   ...
└───css
    │   main.css
    │   normalize.css
└───js
    │   main.js
    │   plugins.js
    └───adon
└───stylesheets
    │   _heaven.scss
    │   _hell.scss
    │   _pillar.scss
    │   judge.scss
    │   settings.scss
    │   theme.scss
    └───aside
        │   _hell.scss
    └───... <many more>
        │   ....scss
    └───ul
        │   _pillar.scss
└───test
    │   stylesheets.js
    │   stylesheets.scss
    └───functions
        │   _i_am.scss
    └───mixins
        │   _i_am.scss
    └───suites
        │   adonTesterSuite.js
```

### Seeing is Believing

```shell
gulp
```

- Type `gulp`.
- ...gulp builds the stylesheets for this project...
- ...gulp loads the `index.html` into the browser...
- ...gulp watches, benevolently: it will rebuild and reload when you make any changes.

### What gulp makes

```shell
project
└───css
    │   genesis.css
    │   genesis.min.css
└───dist
    └───css
        │   genesis.min.css
    └───js
        │   adons.js
        │   main.js
```

- `css/genesis.css`

  - SASS compiled output of your `stylesheets/judge.scss` file.

- `css/genesis.min.css`

  - Minified output of your `css/genesis.css` file.

- `dist/css/genesis.min.css`

  - SASS compiled output of `normalize.css` + `css/genesis.css` files. Users can import this into their non-eliosin projects.

- `dist/js/adons.js`

  - Uglified compiled output of the `js/adon/*.js` files ('plugins' surrogate). `index.hml` imports `dist/js/adons.js` after jquery.

- `dist/js/main.js`

  - Uglified compiled output of `js/main.js`. This script is imported at the bottom of your index.hml.

Other eliosinners should add your `stylesheets/theme.scss` to their `theme.scss` file. `index.hml` imports `dist/js/main.js` before `</body>`.

### What we learnt

1. What **generator-sin** does.

2. What `yo sin` creates.

3. What `gulp` gets up to.

That's it! Now you're ready to start working on your theme, so let's go ahead and make a start.

- Open the project up in your text editor.

## Exercise 2 Changing eliosin LAYOUT settings

- Edit `stylesheets/settings.scss`.

```scss
$god-space: 2%; // the horizontal padding in all the prongs.
$heaven-width: 25%; // prong width right (as a percentage to help scaling).
$hell-width: 25%; // prong width left (as a percentage to help scaling).
$personal-space: 6px; // the vertical padding between believers.
```

- Try changing them one at a time and saving each time to see how each change affects the layout.

### What we learnt

1. How to change the page layout.

## Exercise 3 Change eliosin TEXT settings

```scss
$god-h1: 250%; // font size of h1.
```

- Notice how changing the size of `$god-h1` changes the scale of all headings?

  - All heading fontSizes are distributed between `$god-h1` and `$god-h6`.

  - That's what we call _fibonacciness_ because the sizes are weighted using that famous numerical sequence.

    - Did you know: `h6` happens to be body text size @ 100%

- Change the `$god-h6` the settings shown.

```scss
$god-h6: 100%; // font size of h6.
```

- Notice how changing the size of `$god-h6` doesn't change the `h1` size, but does rescale the other headings?

- Choose a couple of different [fonts](https://fonts.google.com)

- Change the `$god-head` and `$god-body` settings.

```scss
$god-p: 100%; // font size of paragraph text.
$god-spoke: $god-p * 1.2; // standout with big text.
$god-head: "Montserrat", sans-serif; // for heading level text.
$god-body: "Amiri", sans-serif; // for body level text.
```

- Add the link to the `head` area of `index.html` page.

```html
<link
  href="https://fonts.googleapis.com/css?family=Montserrat:100|Amiri"
  rel="stylesheet"
/>
```

### What we learnt

1. How to change fonts and sizes.

## Exercise 4 Change eliosin COLOR settings

Next we're going improve the level of separation between the pillar and outer prongs and for that we'll need an awesome colour scheme. I generally like to make hell darker. From the color scheme, choose which color will be the primary color known as `$god-color`.

```scss
$heaven-color: #007f00;
$pillar-color: black;
$hell-color: #9ebdbd;
$god-color: $heaven-color;
$god-blushed: lighten($hell-color, 25);
```

- I'm stealing color schemes from here:

  - [50 Best Website Color Schemes](https://designshack.net/articles/trends/best-website-color-schemes)

    - It isn't compulsory.

`$god-color` should be used widely in your theme files when you want your tagName style to pick out the main colors.

`god-flushed` should be used widely as a light background, for :active highlights, for muted text, etc.

Ideally you only reference `$heaven-color` `$pillar-color` `$hell-color` for tagNames in those _prongs_.

### What we learnt

1. How to change the color scheme.
2. How to choose a primary color called `$god-color`.
3. How to choose a primary background tint called `god-flushed`.

## Exercise 5 Change theme of Pillar, Heaven and Hell

The `_heaven.scss`, `_hell.scss` and `_pillar.scss` files can be used to style all the tagnames in a _prong_. This is particularly useful for the _pillar_. Just make sure they are referenced in your `_theme.scss` file.

- Edit `stylesheets/_pillar.scss`.

- justify the text of all the tagNames in the _pillar_.

```scss
#{$pillar} {
  text-align: justify;
}
```

- Edit `stylesheets/_heaven.scss`.

- Use the `colorize_god` mixin to apply a background and color.

```scss
#{$heaven} {
  @include colorize_god($god-blushed, $pillar-color);
}
```

- Edit `stylesheets/_hell.scss`.

- Use **god**'s `colorize_god` mixin to apply a background and color.

```scss
#{$hell} {
  @include colorize_god($god-blushed, $god-color);
}
```

### What we learnt

1. How to theme all the tagNames in the _pillar_
2. How to theme all the tagNames in the _heaven_
3. How to theme all the tagNames in the _hell_
4. How to use a mixin. [More mixins](/eliosin/god/mixins.html)

## Exercise 6 Change heading themes

- Edit `stylesheets/theme.scss`.

- Set the color of the heading tags where the font is being set:

```scss
h1,
h2,
h3,
h4,
h5,
h6 {
  color: $hell-color;
}
```

- While you're in the `theme.scss` file, notice how the files we edited above are already referenced? The `yo sin` generator did that for you.

### What we learnt

1. How to theme all the heading tagNames `h1` to `h6`

### Freetime

- Play with the settings some more.

- NB: While you're looking at the `settings.scss` file, notice the way the `$pillar`, `$heaven` and `$hell` settings have also been set up. Leave these for now - we cover them in the next tutorial.

- [More about settings](/eliosin/god/settings.html)

## Exercise 7 Change the sinner positions

> pillar hell and heaven tag shall be siblings of the same container **3rd commandment**

The 3rd command is an important aspect of eliosin html pages. Having all the tags as children of body makes it very easy to switch blocks of content in and out. An eliosin website is little more than an xml stream of articles. But we do need to mind the 6th commandment.

> No _hell_ _heaven_ shall be laid-out higher on the page than the _following_ sibling _pillar_ in the HTML source (examples coming). **6th commandment**

The 6th command states the position of tagNames is important. _pillar_ will just be piled up on top of eachother down the middle of the page - literally a giant pillar in the centre of the webpage. _hell_ and _heaven_ tagNames will float left and right held adjacent to the next _pillar_ tagName below it. Exercise 7 will demonstrate this.

- Open the `index.html` page.

- Notice that all tags are children of `body`?

- Move the `aside` and `h6` block directly above `ul` as shown.

```html
<aside>...</aside>

<h6>...</h6>

<ul>
  ...
</ul>
```

- Move the `details` and `summary` block directly above the `dl` block.

```html
<details>...</details>

<summary>...</summary>

<dl>...</dl>
```

- Make sure all tags remain siblings as per **3rd commandment**

- Save and look at the webpage in the browser.

### What we learnt

1. Notice how the tags `aside` `h6` `details` and `summary` sit _alongside_ the `ul` and `dl` tags which are _below_ them in the HTML?

2. Yeah, I noticed.

3. Excellent. This QuickStart is over.

4. I don't notice!

5. Sorry. **god** is not for you.

## Appendix: Core files

### `stylesheets/_<prong>.scss`

A style for all `tagNames` sitting in that _prong_.

### `stylesheets/settings.scss`

**elioGod**'s basic settings.

### `stylesheets/theme.scss`

This file is the exportable version that other sin libraries can `@import` when the want to borrow your theme. Only tagNames from your theme should be here!

### `stylesheets/judge.scss`

This file is 'gulped' to build the css file for this eliosin theme, and should `@import` everything else your theme requires, including **god** and your `theme.scss`.

## Tidy up

- Press `CTRL` and `C` to stop the browser.

- Type `npm run prettier` to make your work `prettier`.

- Type `npm run test`. You haven't written any tests yet, but there are 3 tests already which should always pass.

## Next

You have now seen how **god** works to control the layout and style of the three _prongs_.

Next you'll be using **eve**'s _patterns_ for theming individual tagNames.

- [eve Quickstart](/eliosin/eve/quickstart.html)
