# The Prong Cookbook 1: The blogging app

## Hypothetical:

Let's take the example of a hypothetical blogging app. We're expecting standard HTML from the CMS.

Each page will show a main article, but the page will also display a list of sub-articles in full or summary below the main article. The articles will be one long - infinity loading - feed.

We will use the `article` tag **the elioWay**. **god** styles tagNames, so choose your parent tagName wisely in your template.

## Implementation:

- We'll be coding in a standard express/mongoose setup with a templating system.
- Freed of HTML fluff, we will render the articles directly into the `body`.
- We will render sub-articles directly into the `article` container inside `body`.

So the nodejs app need only inject content into the DOM at two locations: `body` and `body>article`. A CMS will be available to the blog owner and we will make sure only the `$pillar` tagNames can be created by its HTMLEditor. This means that the CMS itself only gives the user access to the _pillar_. The app will have the ability to generate _hell_ and _heaven_ tags though: so bear that in mind. Our app will generate that content dynamically...

## Example settings file

```shell
$container: (
  article,
);
$pillar: (
    h1, h2, h3, ul, ol, dl, p, # are in the pillar
);
$heaven: (
    header, blockquote, h4, # are in heaven
);
$hell: (
     menu, nav, aside, # are in hell
);
```

**The rationale:**

The root tagName of any CSS template will target one of the tagNames in these settings. **eve** helps us do this. So our app will pump out the following html per page:

```asciiart
body
  article
    h1  TitleArticle1
    h2  SubTitleArticle1
    p   blurbArticle1
    p   blurnArticle1
    article
      h1  TitleSubArticle2
      h2  SubTitleSubArticle2
      p   blurbSubArticle2
      p   blurbSubArticle2
    article
      h1  TitleArticle3
      h2  SubTitleSubArticle3
      p   blurbSubArticle3
      p   blurbSubArticle3
    article
      h1  TitleArticle4
      h2  SubTitleSubArticle4
      p   blurbSubArticle4
      p   blurbSubArticle5
```

Technically it won't make any difference to **god** . It considers all these tags equal, but it will help our app to be able to treat the two article parents separately - targeting the main content and the listed sub content separately.

The app would also inject `menu`s, `nav`s, `aside`s `blockquote`s, etc into the DOM for site navigation, breadcrumps, social media icons, hints relating to the content, etc. With these tagNames being in _heaven_ or _hell_ content would automatically "drift" into the gutters either side of the _pillared_ content. If I add the "fixed" style to `menu`, I know where it will be fixed: In _heaven_ or _hell_.
